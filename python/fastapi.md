### FastAPI

FastAPI 是python的一个高性能api 框架

#### 安装

```
pip install FastAPI
```

使用

```python
from fastapi import FastAPI

app = FastAPI()
items = {}

# 程序启动的时候调用的方法
@app.on_event("startup")
async def startup_event():
    
    print("server is start up...")
    
# 程序关闭的时候调用
@app.on_event("shutdown")
def shutdown_event():
    with open("log.txt", mode="a") as log:
        log.write("Application shutdown")


# api get 
@app.get("/items/")
async def read_items():
    return [{"name": "Foo"}]
```



