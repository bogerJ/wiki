1.  下载mysql5.5.62，URL：https://downloads.mysql.com/archives/community/ 我这里下载的是64位版本 

2.   卸载自带的mariadb和mysql 

   1.  检查是否安装了mariadb和mysql，有时候默认安装了 

   ```
   rpm -qa | grep mariadb
   rpm -qa | grep mysql
   ```

   2. 如果没有，就可以安装mysql，如果有，需要先卸载（remove后为上面命令查询到的内容，全文件名，我这里没有，没法展示） 

   ```
   yum remove mariadb-xxx
   ```

3. 解压文件，修改目录名方便配置 ,解压，并移动到某个文件夹

   ```shell
   tar -zxvf mysql-5.5.62-linux-glibc2.12-x86_64.tar.gz 
   mv mysql-5.5.62-linux-glibc2.12-x86_64 /usr/local/mysql
   cd /usr/local/mysql
   ```

4.  添加mysql用户

   ```
   useradd -s /bin/nologin mysql 
   ```

5. 创建数据目录，并改变目录权限

   ```
   mkdir -P /data/mysql
   chown -R mysql:mysql /data/mysql
   ```

6. 修改配置文件，安装目录下的support-files 里面有配置文件复制一份到 /etc/my.cnf，

   复制mysql.server 到 /etc/init.d/mysqld

   ```
   cp /usr/local/mysql/support-files/my-large.cnf /etc/my.cnf
   cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysqld
   ```

7. 修改mysqld 权限，并修改数据目录配置

   ```
   chmod 755 /etc/init.d/mysqld 
   vi /etc/init.d/mysqld   #修改datadir
   ```

8. 初始化 mysql  

   ```
   /usr/local/mysql/scripts/mysql_install_db --user=mysql --datadir=/data/mysql
   ```

9. 设置开机自启

   ```
   chkconfig --add mysqld 
   chkconfig mysqld on 
   service mysqld start 
   ```


10. 设置密码，权限

    ```
    set password for root@localhost = password('123'); #改密码可以不执行
    grant all privileges on *.* to root@'%' identified by '123'; #授权并设置密码
    flush privileges;#刷新权限
    ```

    