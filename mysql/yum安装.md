#### 1 下载rpm 文件

```
wget -i -c http://dev.mysql.com/get/mysql57-community-release-el7-10.noarch.rpm
```



#### 2 安装MySql

```
yum -y install mysql57-community-release-el7-10.noarch.rpm
```

3 安装服务

```
yum -y install mysql-community-server
```

4 启动服务

```
systemctl start mysqld.service
```

5 QA:

* Q:如下报错

```
The GPG keys listed for the "MySQL 5.7 Community Server" repository are already installed but they are not correct for this package.
Check that the correct key URLs are configured for this repository.


 Failing package is: mysql-community-server-5.7.38-1.el7.x86_64
 GPG Keys are configured as: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

```

* A：执行如下命令

  ```
  rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022
  ```

  