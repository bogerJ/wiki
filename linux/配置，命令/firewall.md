#### firewall 防火墙服务简述与安装
---
1、Centos7 默认的防火墙是 firewall，替代了以前的 iptables  
2、firewall 使用更加方便、功能也更加强大一些  
3、firewalld 服务引入了一个信任级别的概念来管理与之相关联的连接与接口。它支持 ipv4 与 ipv6，并支持网桥，采用 firewall-cmd (command) 或 firewall-config (gui) 来动态的管理 kernel netfilter 的临时或永久的接口规则，并实时生效而无需重启服务。  
4、查看 firewall  版本：firewall-cmd --version  

##### firewall 防火墙安装  
1. 像使用 iptables 一样，firewall 同样需要安装  
2. 需要注意的是某些系统已经自带了 firewal l的，如果查看版本没有找到，则可以进行 yun 安装   
3. 安装指令： yum install firewalld  

##### firewalld 服务基本使用

| 目的 | 命令   |
| --------   | -----  |
| 查看防火墙状态      | systemctl status firewalld   |
| 查看防火墙状态     | systemctl status firewalld |
| 关闭防火墙，停止 firewall 服务 | systemctl stop firewalld |
| 开启防火墙，启动 firewall 服务 | systemctl start firewalld |
| 重启防火墙，重启 firewall 服务 | systemctl restart firewalld |
| 查看 firewall 服务是否开机启动 | systemctl is-enabled firewalld |
| 开机时自动启动 firewall 服务 | systemctl enable firewalld.service |
| 开机时自动禁用 firewall 服务 | systemctl disable firewalld.service |

##### firewalld-cmd 防护墙命令使用

1. 上面所说的 firewall 可以看成整个防火墙服务，而 firewall-cmd 可以看成是其中的一个功能，可用来管理端口
2. 查看 firewall-cmd 状态，即查看 firewall 防火墙程序是否正在运行： firewall-cmd --state  
```
[root@localhost ~]#  firewall-cmd --state
running
[root@localhost ~]# 
```
3. 查看已打开的所有端口，firewall-cmd --zone=public --list-ports 

```shell
[root@localhost ~]# firewall-cmd --zone=public --list-ports
6379/tcp 22122/tcp 23000/tcp 8080/tcp 8888/tcp 9502/tcp 6662/tcp 9999/tcp 7002/tcp 6661/tcp 6688/tcp 6667/tcp 6689/tcp 8000/tcp 6663/tcp 9070/tcp 9089/tcp 9988/tcp 9222/tcp 4444/tcp
[root@localhost ~]# 
```

4. 开启指定端口
    + 开启一个端口：firewall-cmd --zone=public --add-port=80/tcp --permanent    （--permanent 永久生效，没有此参数重启后失效）
    + 重新加载 firewall，修改配置后，必须重新加载才能生效：firewall-cmd --reload   
```
[root@localhost ~]# firewall-cmd --zone=public --list-port
9876/tcp 8090/tcp 80/tcp 8080/tcp
[root@localhost ~]# firewall-cmd --zone=public --add-port=3307/tcp --permanent
success
[root@localhost ~]# firewall-cmd --reload
success
[root@localhost ~]# firewall-cmd --zone=public --list-port
9876/tcp 8090/tcp 80/tcp 8080/tcp 3307/tcp
[root@localhost ~]# 
```
4. 关闭指定端口
    + 关闭 9876 端口：firewall-cmd --zone=public --remove-port=9898/tcp --permanent（--permanent 表示永久生效，没有此参数重启后失效）  
    + 重新加载 firewall，修改配置后，必须重新加载才能生效：firewall-cmd --reload  
```
[root@localhost ~]# firewall-cmd --zone=public --list-ports
9876/tcp 8090/tcp 80/tcp 8080/tcp
[root@localhost ~]# firewall-cmd --zone=public --remove-port=9876/tcp --permanent
success
[root@localhost ~]# firewall-cmd --reload
success
[root@localhost ~]# firewall-cmd --zone=public --list-ports
8090/tcp 80/tcp 8080/tcp
[root@localhost ~]#
```
##### public.xml 文件修改防火墙端口
1. firewall-cmd对端口的操作，如开放端口等信息，都放在在"/etc/firewall/zones/public.xml"中记录
2. 所以直接修改此文件也是可以的

