#### nginx 代理 nexus 配置
---
*参考链接 https://www.jianshu.com/p/1e0f190637e5*  
关键配置
```
location / {
            proxy_pass   http://ws_nexus;
            # nexus支持
            proxy_set_header Host $host:$server_port; #需要加$server_port 否则访问仓库400
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            client_max_body_size    1000m;
        }
```
*还需要加上文件请求大小的限制*

