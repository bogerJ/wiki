##### 修改默认端口  
例如：修改ssh端口号为3389  
+ vim /etc/ssh/sshd_config   
+ port 22 下面添加 port   3389  
```
# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 10017
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
```
+ systemctl restart sshd

##### 其他安全设置：禁止root远程登录，最大失败次数3  
```
# Authentication:

#LoginGraceTime 2m
PermitRootLogin yes
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10
```