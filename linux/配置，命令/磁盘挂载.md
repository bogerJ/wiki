Linux centos 磁盘挂载

### 查看磁盘情况	 `fdisk -l`

```shell
[root@vm12345-0417747 ~]# fdisk -l

Disk /dev/vda: 42.9 GB, 42949672960 bytes, 83886080 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x000ee330

   Device Boot      Start         End      Blocks   Id  System
/dev/vda1            2048     8390655     4194304   82  Linux swap / Solaris
/dev/vda2   *     8390656    83886046    37747695+  83  Linux

Disk /dev/vdb: 214.7 GB, 214748364800 bytes, 419430400 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x00000000

   Device Boot      Start         End      Blocks   Id  System
/dev/vdb1            2048   419430399   209714176   83  Linux
[root@vm12345-0417747 ~]#

```

可以看到有两块磁盘 ，dev /vda	/dev/vdb

### 磁盘分区

现在对 `/dev/vdb` 磁盘进行分区

`fdisk /dev/dbd` 进行磁盘分区

```shell
[root@vm12345-0417747 ~]# fdisk /dev/vdb
Welcome to fdisk (util-linux 2.23.2).

Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): m
Command action
   a   toggle a bootable flag
   b   edit bsd disklabel
   c   toggle the dos compatibility flag
   d   delete a partition
   g   create a new empty GPT partition table
   G   create an IRIX (SGI) partition table
   l   list known partition types
   m   print this menu
   n   add a new partition
   o   create a new empty DOS partition table
   p   print the partition table
   q   quit without saving changes
   s   create a new empty Sun disklabel
   t   change a partition's system id
   u   change display/entry units
   v   verify the partition table
   w   write table to disk and exit
   x   extra functionality (experts only)

Command (m for help):

```

进入磁盘管理，m 帮助

添加分区: n 添加分区

```
Partition type:
   p   primary (1 primary, 0 extended, 3 free)
   e   extended
Select (default p):
```

选择 p 主分区 r 扩展分区，输入p 

```
Select (default p): p
Partition number (2-4, default 2):
```

然后提示选,设置分区编号，可用2-4 默认2，直接默认值，下一步，下一步，下一步，都选择默认值

最后 wq 保存分区

### 分区格式化

查看分区 `fdisk -l` ,可以看到一个新的分区 `/dev/vdb1`

```shell
Disk /dev/vdb: 214.7 GB, 214748364800 bytes, 419430400 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x00000000

   Device Boot      Start         End      Blocks   Id  System
/dev/vdb1            2048   419430399   209714176   83  Linux
```

对这个分区进行格式化, `ext4` 是分区类型

```shell
mkfs -t ext4 /dev/vdb1
```

### 挂载目录

创建一个目录，我在根目录创建了一个 `/data` 目录,

```shell
mkdir /data
```

挂载目录 把分区  `/dev/vdb1` 挂载到  /data 目录

```shell
mount /dev/vdb1 /data
```

### 设置开机自动挂载

磁盘的UUID（universally unique identifier）是Linux系统为存储设备提供的唯一的标识字符串。

执行 `blkid`  命令，查询磁盘分区的UUID，例如查询`/dev/vdb1`

```
[root@vm12345-0417747 ~]# blkid /dev/vdb1
/dev/vdb1: UUID="3a7d1c7c-5ee4-4a92-9cbc-16d1a2ee7491" TYPE="ext4"
```

编辑文件`/etc/fstab`，在文件末尾添加一行

```
UUID=3a7d1c7c-5ee4-4a92-9cbc-16d1a2ee7491 /data ext4 defaults 0 2
```

参数解释：

- UUID=bb84333a-6a0d-4285-a14c-cf8b5da88d61：要挂载的磁盘分区的UUID
- /home/eason/data：挂载目录
- ext4：分区格式为ext4
- defaults：挂载时所要设定的参数(只读，读写，启用quota等)，输入defaults包括的参数有(rw、dev、exec、auto、nouser、async)
- 0：使用dump是否要记录，0为不需要，1为需要
- 2：2是开机时检查的顺序，boot系统文件为1，其他文件系统都为2，如不要检查就为0