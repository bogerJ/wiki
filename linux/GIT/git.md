git branch -a 查看所属分支  * 表示远程分支

```
*localbranch
 master
 release
```

git branch -r 查看远程分支

 

git branch  查看当前分支

git branch -b dev 

git checkout -b dev 新建dev 分支，参数  -b 切换到dev 分支

git push origin dev:dev 本地新建的分支推送到远程

* 删除远程分支（dev）

git push origin :<远程分支名称>

git push origin --delete <远程分支名称>


git stash  存储没有追踪的文件

合并分支

开发分支（dev）上的代码达到上线的标准后，要合并到 master 分支

```
git checkout dev
git pull
git checkout master
git merge dev
git push -u origin master
```

 当master代码改动了，需要更新开发分支（dev）上的代码

```
git checkout master 
git pull 
git checkout dev
git merge master 
git push -u origin dev
```