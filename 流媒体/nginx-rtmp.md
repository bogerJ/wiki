### 安装nginx+rtmp

这里使用docker仓库里`tiangolo/nginx-rtmp`的，直接拉下来运行即可

```bash
# 拉取镜像
docker pull tiangolo/nginx-rtmp
# 根据镜像运行容器
docker run -d -p 1935:1935 --name nginx-rtmp tiangolo/nginx-rtmp
```

推流

rtmp://127.0.0.1:1935/live

拉流 使用vlc

rtmp://127.0.0.1:1935/live