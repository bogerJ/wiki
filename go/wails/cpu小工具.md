#### go wails GUI cpu使用率监控工具

> 做一个桌面的cpu监控工具，用go 语言来获取系统的cpu使用率，然后通过wails 的事件系统，通知界面更新数据，实现cpu使用率的监控。

##### 几个主要的问题点

1. go 程序如何获取cpu使用率
2. 获取的cpu数据如何实时通知界面
3. 如何进行可视化展示

##### 创建 go wails 项目

通过命令创建go wails 项目模版，选择 vue js 模版

##### 构建界面

做一个如下的界面，用echarts 做可视化展示，写一个简单的仪表盘图表，直接按照官方的例子抄过来即可。这地方窗口是半透明的

![image-20230904184536207](C:\Users\a\AppData\Roaming\Typora\typora-user-images\image-20230904184536207.png)

添加一个echarts 仪表盘类型的图表

```
<div style="--wails-draggable: drag">
	<div id="myChart"></div>
</div>
```

js 代码

```js
initOptions() {
    this.options = {
        tooltip: {
            formatter: "{a} <br/>{b} : {c}%",
        },
        series: [
            {
                name: "Pressure",
                type: "gauge",
                progress: {
                    show: true,
                },
                detail: {
                    valueAnimation: true,
                    formatter: "{value}",
                },
                data: [
                    {
                        value: 50,
                        name: "SCORE",
                    },
                ],
            },
        ],
    };
},
    initCharts() {
        this.chart = echarts.init(document.getElementById("myChart"));
        this.chart.setOption(this.options);
    },
```

##### 添加事件监听

使用wails提供的事件监听来实现从go 到 js 的消息通知，从而实现实时通知界面更新cpu使用率，

使用到了 wails 提供的 EventsOn 添加监听，收到数据以后，更新图表

```js
AddEvent() {
    let that = this;
    EventsOn("sysInfoPublish", (msg) => {
        that.chart.setOption({
            series: [
                {
                    data: [
                        {
                            value: msg,
                        },
                    ],
                },
            ],
        });
        console.log(msg);
    });
},
```

##### go 获取cpu 使用率

go 程序利用开源的一个库获取cpu使用率

```
// 获取cpu信息的库 github.com/shirou/gopsutil/cpu
```

获取cpu并通知js 的代码如下，获取以后，通过事件系统，发布消息，js 收到消息以后会更新界面

```go
func (a *MonitorApp) GetSysInfo() {
	go func(ctx context.Context) {
		for {
			percent, err := cpu.Percent(time.Second*2, false)
			if err != nil {
				log.Println(err.Error())
				return
			}
			runtime.EventsEmit(ctx, "sysInfoPublish", float64(int(percent[0]*100))/100)
		}
	}(a.ctx)
}
```

##### 总结

都是比较简单的东西，本来想着能做个全透明的界面，看了下wails 的文档，只有半透明窗口。主要的内容还是如何获取cpu信息，和使用wails的事件机制，实时更新界面。

