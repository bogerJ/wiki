## go-gtk 安装完后提示缺少依赖

入坑gogui 按照教程搭环境，参考链接

 https://blog.csdn.net/tennysonsky/article/details/79221507 

安装过程中msys2 自带的源有点慢，改下源, windows下修改

编辑下面的文件，添加的行要放到其他行的最前面，下载的时候是按照源的顺序的。

安装目录\etc\pacman.d\mirrorlist.mingw32

```
Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/mingw/i686
```
安装目录\etc\pacman.d\mirrorlist.mingw64
```
Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/mingw/x86_64
```

安装目录\etc\pacman.d\mirrorlist.msys
```
Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/msys/$arch
```



环境安装下来没啥问题，需要注意一点，环境变量

![1588932807195](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\1588932807195.png)

这个是个win10的比较明显

**一定要把c:\msys64\mingw64\bin 配置到 c:\msys64\usr\bin 前面**

要不编译go-gtk程序会报错，找不到依赖包

