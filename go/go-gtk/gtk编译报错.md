# 使用go编译go-gtk，直接编译会出现错误

1.  

   pkg-config --cflags -- gdk-2.0 gthread-2.0

2.  

   pkg-config --libs -- gdk-2.0 gthread-2.0

```html
go build github.com/mattn/go-gtk/gdk: invalid flag in pkg-config --libs: -Wl,-luuid
```

#### 爬文后，发现[这里](https://github.com/golang/go/issues/23749)的讨论，让我顿悟：

 

修改：C:\msys64\mingw64\lib\pkgconfig\gdk-2.0.pc，如下：

1.  

   Libs: -L${libdir} -lgdk-${target}-2.0 -lgdi32 -limm32 -lshell32 -lole32 -luuid

2.  

   Cflags: -I${includedir}/gtk-2.0 -I${libdir}/gtk-2.0/include

3.  

   LDFLAGS: -Wl

将原本在Libs中的-Wl删除，增加一行即可：

```html
LDFLAGS: -Wl
```

 

将原本在Libs中的-Wl删除，增加一行即可：

LDFLAGS: -Wl

修改后，同时将其他pc文件一同修改：

![img](https://img-blog.csdn.net/201806070945044)