## msys2 

#### 1.  更换源

   ​	参考网站：https://mirror.tuna.tsinghua.edu.cn/help/msys2/

   ​    pacman 的配置(文件都在安装目录下)

   编辑 `/etc/pacman.d/mirrorlist.mingw32` ，在文件开头添加：

   ```
   Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/mingw/i686
   ```

   编辑 `/etc/pacman.d/mirrorlist.mingw64` ，在文件开头添加：

   ```
   Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/mingw/x86_64
   ```

   编辑 `/etc/pacman.d/mirrorlist.msys` ，在文件开头添加：

   ```
   Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/msys/$arch
   ```

   然后执行 `pacman -Sy` 刷新软件包数据即可。

#### 2. 安装 gcc

```
pacman -S gcc
```

#### 3. 安装make

```
pacman -S make
```

