gin-swagger

## 下载安装cmd/swag命令工具包

```
go get -u github.com/swaggo/swag/cmd/swag
```

> go mod 模式 下载的包在 %GOPATH%\pkg\mod\github.com/swaggo/swag/cmd
>
> 需要执行 go install 会在  %GOPATH%\bin 下生成 swag.exe 

## 执行初始化命令

```
swag init  // 注意，一定要和main.go处于同一级目录
```

> 会在目录下 

项目配置示例

```
package main

import (
    "apiwendang/controller"
    _ "apiwendang/docs"
    "github.com/gin-gonic/gin"
    swaggerFiles "github.com/swaggo/files"
    ginSwagger "github.com/swaggo/gin-swagger"
)


// @title Docker监控服务
// @version 1.0
// @description docker监控服务后端API接口文档

// @contact.name API Support
// @contact.url http://www.swagger.io/support

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host 127.0.0.1:9009
// @BasePath
func main() {
    r := gin.New()

    r.Use(Cors())
    //url := ginSwagger.URL("http://localhost:8080/swagger/doc.json") // The url pointing to API definition
    r.POST("/test/:id", controller.Test)
    r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

    r.Run(":9009")
}


func Cors() gin.HandlerFunc {
    return func(c *gin.Context) {
        c.Header("Access-Control-Allow-Origin", "*")
        c.Next()
    }
}
```

