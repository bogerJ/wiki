## hystrix 熔断器

> 熔断器是为了保护被调方健康的一种方式。通过错误率，超时，并发等机制来使第三方处于一个健康且提供性能最佳的方式。hystrix 是比较通用的熔断器库。以下为介绍该熔断器源码以及处理思想。

1. Timeout: 执行command的超时时间。默认时间是1000毫秒
2. MaxConcurrentRequests：command的最大并发量 默认值是10
3. SleepWindow：当熔断器被打开后，SleepWindow的时间就是控制过多久后去尝试服务是否可用了。默认值是5000毫秒
4. RequestVolumeThreshold： 一个统计窗口10秒内请求数量。达到这个请求数量后才去判断是否要开启熔断。默认值是20
5. ErrorPercentThreshold：错误百分比，请求数量大于等于RequestVolumeThreshold并且错误率到达这个百分比后就会启动熔断 默认值是50
6. 当然如果不配置他们，会使用默认值

功能模块

###  限流

​	采用令牌算法，拿到令牌开始工作，执行完成返回令牌。等不到令牌时返回maxconcurent。

### 熔断开关控制

#### 熔断器状态判断

​	当请求数量大于 统计桶内(10s 间隔)的 大于 RequestVolumeThreshold 并且错误率大于 ErrorPercentThreshold 时熔断器打开。

### micro 集成

通过 micro plugin 进行集成 hystrix(断路器)，

```go
type Breaker struct {
   Timeout                int 
   MaxConcurrentRequests  int    
   RequestVolumeThreshold int    
   SleepWindow            int    
   ErrorPercentThreshold  int    
   Name                   string 
   StreamServerPort       string 
}

//定义一些参数，可以通过启动micro 的时候传参
func (l *Breaker) Flags() []cli.Flag {
   return []cli.Flag{}
}
func (l *Breaker) Commands() []cli.Command {
   return nil
}

//处理程序 会在每次请求的时候调用
func (l *Breaker) Handler() plugin.Handler {
   return func(handler http.Handler) http.Handler {
      return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
         name := r.Method + "-" + r.RequestURI
         logger.Info("breaker:" + name)

         conf := hystrix.CommandConfig{
            Timeout:                l.Timeout,
            MaxConcurrentRequests:  l.MaxConcurrentRequests,
            RequestVolumeThreshold: l.RequestVolumeThreshold,
            SleepWindow:            l.SleepWindow,
            ErrorPercentThreshold:  l.ErrorPercentThreshold,
         }

         hystrix.ConfigureCommand(name, conf)
         err := hystrix.Do(name, func() error {
            sct := &statuscode.StatusCodeTracker{ResponseWriter: w, Status: http.StatusOK}
            handler.ServeHTTP(sct.WrappedResponseWriter(), r)

            if sct.Status >= http.StatusBadRequest {
               str := fmt.Sprintf("status code %d", sct.Status)
               logger.Error(str)
               return errors.New(str)
            }
            return nil
         }, nil)
         if err != nil {
            logger.Error("hystrix breaker err: ", err)
            http.Error(w,err.Error(),http.StatusBadGateway) //错误返回
            return
         }
      })
   }
}

//初始化数据,程序启动的时候会调用这个方法，可以在这里初始化一些参数
func (l *Breaker) Init(ctx *cli.Context) error {
   //初始化配置  从配置中心取配置
   logger.Debug("初始化配置...")
   hystrixStreamHandler := hystrix.NewStreamHandler()
   hystrixStreamHandler.Start()
   go http.ListenAndServe(net.JoinHostPort("", l.StreamServerPort), hystrixStreamHandler)

   return nil
}
func (l *Breaker) String() string {
   return l.Name
}

//调用这个方法 new 插件，也可以在启动的时候这样子写
func NewPlugin() plugin.Plugin {
   return &Breaker{
      Name:                   "breaker-hystrix",
      Timeout:                hystrix.DefaultTimeout,
      MaxConcurrentRequests:  hystrix.DefaultMaxConcurrent,
      RequestVolumeThreshold: hystrix.DefaultVolumeThreshold,
      SleepWindow:            hystrix.DefaultSleepWindow,
      ErrorPercentThreshold:  hystrix.DefaultErrorPercentThreshold,
      StreamServerPort:"81",
   }
}
```

使用,只需要在启动的时候注册进去就好了

```go
func init() {
	plugin.Register(token_jwt.NewPlugin())
	plugin.Register(breaker.NewPlugin()) //注册熔断器 plugin
	log.InitZap()
}
func main() {
	cmd.Init()
}
```

## Hystrix Dashboard 熔断器监控界面 docker 部署

```
docker run --name hystrix-dashboard -d -p 8081:9002 mlabouardy/hystrix-dashboard:latest
```

访问监控界面：http://localhost:8082/hystrix

然后输入 http://192.168.0.105:81/hystrix.stream

进入监控	Moniter Stream



