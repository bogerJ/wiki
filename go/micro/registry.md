registry ,启动注册服务，然后向注册服务进行注册，发送如下数据进行注册即可

```json
{
    "name": "go.micro.api.weather",
    "version": "1.0",
    "endpoints": [],
    "nodes": [
        {
            "address": "192.168.0.108",
            "id": "go.micro.api.weather.python"
        }
    ]
}
```

```json
{
	"endpoint":"Registry.Register",
	"service":"go.micro.registry",
	request:{
        "name": "go.micro.api.weather",
        "version": "1.0",
        "endpoints": [],
        "nodes": [
            {
                "address": "192.168.0.108",
                "id": "go.micro.api.weather.python"
            }
        ]
    }
}
```



Deregistry

```
{
	"name": "go.micro.api.weather",
        "version": "1.0",
        "endpoints": [],
        "nodes": [
            {
                "address": "192.168.0.108",
                "id": "go.micro.api.weather.python"
            }
        ]
}
```



