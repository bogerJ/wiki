### micro api 

micro api 介绍

```shell
$ micro api --help
NAME:
   micro.exe api - Run the api gateway

USAGE:
   micro.exe api [command options] [arguments...]

OPTIONS:
   --address value    Set the api address e.g 0.0.0.0:8080 [%MICRO_API_ADDRESS%]
   --handler value    Specify the request handler to be used for mapping HTTP requests to services; {api, event, http, rpc} [%MICRO_API_HANDLER%]
   --namespace value  Set the namespace used by the API e.g. com.example.api [%MICRO_API_NAMESPACE%]
   --resolver value   Set the hostname resolver used by the API {host, path, grpc} [%MICRO_API_RESOLVER%]
   --enable_rpc       Enable call the backend directly via /rpc [%MICRO_API_ENABLE_RPC%]


```

使用的时候需需要指定 handler 

```
micro --registry=etcd --registry_address=127.0.0.1:2379 api --heandler=http --address=127.0.0.1:8081
```
