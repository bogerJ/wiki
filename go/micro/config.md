## micro config

> ​	micro 的配置文件是内置的一个配合管理系统，config 可以从 etcd ,consul,或者文件中加载配置信息，使用的时候也比较方便

### source

config 需要从source 来加载配置，micro 已经实现了12 中source 加载的方法，也就是说可以从12 种不同的媒介中载入配置，当然也可以自己写插件来实现载入配置，这里列举出几个会比较常用的配置加载方式

1. etcd
2. consul
3. file
4. env
5. cli
6. url

在项目中使用到的是etcd,这里就用etcd 的使用举例

配置加载的方法，使用起来是比较简单

1. 创建source

```go
source := etcd.NewSource(
		etcd.WithAddress(address),
		)
```

2. 加载配置

```go
l.conf=config.NewConfig()
	err:=l.conf.Load(source)
	if err!=nil {
		logger.Fatal(err)
	}
```

3. 获取配置

```go
value:=l.conf.Get(strings.Split(token_path,"/")...).Bytes()
```

从etcd加载配置需要注意的问题

etcd中存配置的时候需要时json格式存进去的，配置的路径要注意，micro默认的配置路径是

```go
/micro/config
```

这里举个例子就明白了，比如我们要存jwt 的私钥文件进去要怎么存 ，前面说了，必须要存进去json 格式的才能行，那么就应该是这样子

```go
/micro/config/token:{"private":"123456abc"}
```

怎么取呢 ？看下源码里面取值得方法

```go
config.Config.Get(path ...string)
```

追踪到一个实现

```go
func (j *Json) GetPath(branch ...string) *Json {
	jin := j
	for _, p := range branch {
		jin = jin.Get(p)
	}
	return jin
}
```

可以看到了，其实是根据存值得路径一层层取出来的。

取值的方法

```go
//这样子直接取到了private的值
// 123456abc
value:=l.conf.Get("micro","config","token","private").Bytes()

//或者这样子，取到了token {\"private\":\"123456abc\"}
value:=l.conf.Get("micro","config","token").Bytes()
```

etcd 动态更新,Config里面的watch 方法允许我们订阅配置的变更，从而做出反应

```go
//配置更新的方法
func (l *Token) enableAutoUpdate(path ...string) {
	go func() {
		for {
			w, err := l.conf.Watch(path...)
			if err != nil {
				logger.Error(err)
			}
			v, err := w.Next()
			if err != nil {
				logger.Error(err)
			}

			value := v.Bytes()
			l.PrivateKey=value

			logger.Info("New JWT privateKey:", string(l.PrivateKey))
		}
	}()
}
```

监听指定路径的配置，来动态更新token 私钥



