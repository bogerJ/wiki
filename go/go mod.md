#####  go modules 使用

> modules 管理go 项目依赖需要至少 1.12  ，相对于GOPATH 更加灵活。

##### 1. 开启配置

  go env -w  GO111MODULE="on"
  go env -w  GOPROXY="https://goproxy.cn"

##### 2. 创建一个go mod 模式的项目
  go mod init xxx
  创建后会在目录下生成 go.mod文件，这个文件就是管理go项目依赖的文件

##### 3. 添加依赖
 在目录下执行 go get xxxx ，然后在项目中直接就可以使用了。

 当然也可以采用 go get xxxx@v.1.6.2这样指定版本来添加

 依赖可以在 https://pkg.go.dev/上查找相关的依赖包信息

 go mod tidy  补全清理依赖

##### 4. 升级依赖
  升级 major 版本: go get -u github.com/xxx/xxx
  升级全部依赖的 minor 或 patch 版本: go get -u
  升级全部依赖的 patch 版本: go get -u=patch

##### 5. 使用
   go get 下载的包会在  %GOPATH%\pkg\xxx


