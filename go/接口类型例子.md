### 会旺旺的就是狗，会喵猫的就是猫

Golang 中 Interface 是一种类型，可以理解为Java 中的Object，可以进行传参。

Interface 只定义一种对象的行为规范，不需要实现，由具体的类型来实现规范。

Interface 是一组方法的集合，是`duck-type programming`的一种体现。

鸭子类型解释是 “如果看起来像鸭子，叫起来像鸭子，那么它一定是鸭子。”

> ```text
> If it looks like a duck and quacks like a duck, it must be a duck.
> James Whitcomb Riley
> ```

这里重点关注的是你能做什么

假如我们定义，会旺旺叫的就是狗，会喵猫的就是猫。

那么我们可以如下定义，定义两个接口类型Dog ，Cat 分别定义了一个行为

```go
type Dog interface {
    WangWang ()
}

type Cat interface {
    MiaoMiao ()
}
```

定义两个方法，分别接受Dog,Cat两个类型

```go
// 是条狗
func IsDog(d Dog)  {
    fmt.Println("is dog")
    d.WangWang()
}

// 是只猫
func IsCat(d Cat)  {
    fmt.Println("is cat")
    d.MiaoMiao()
}
```

#### 如何实现接口

只要定义的类型实现，了某个接口定义的所有方法，那么就实现了这个接口

定义三种结构体，如下，分别实现了Dog，Cat

Aim3结构体同时实现了Dog,Cat ，所以，一个接口体可以实现多个接口，可以同时具备多个接口的能力。

所以Aim3 是Dog ,也是Cat

```go
// 动物1
type Aim struct {}
// 这里是，值接受者实现接口
func (a Aim)WangWang()  { fmt.Println("wang wang ")}

// 动物2
type Aim2 struct {}
// 这里是，指针接受者实现接口
func (a *Aim2)MiaoMiao()  {fmt.Println("miao miao ")}

// 动物3
type Aim3 struct {}
func (a Aim3)MiaoMiao()  {fmt.Println("miao miao ")}
func (a Aim3)WangWang()  {fmt.Println("wang wang ")}

```

#### 接下来验证下

```go
 // 动物1
aim:=Aim{}
IsDog(aim)
// IsCat(aim) // 这里会提示，Aim 没有实现 MiaoMiao 方法，不能进行传参
fmt.Println("===========")
// 动物2
aim2:=Aim2{}
IsCat(&aim2)
// 指针类型接收者实现接口，只能通过指针类型传递,
// 值接收者实现的接口，可以传值和指针
fmt.Println("===========")
// 动物3
aim3:=Aim3{}
IsDog(aim3)
IsCat(aim3)
// Aim3 类型分别实现了 WangWang ,MiaoMiao
// 所以，IsDog,IsCat 两个方法都可传进去
```

输出结果

```shell
is dog
wang wang 
===========
is cat
miao miao 
===========
is dog
wang wang 
is cat
miao miao 
```



