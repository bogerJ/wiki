Go Https 证书的问题

#### 问题描述

  项目中用goalng做了个客户端，客户端发起https请求，以前旧的证书一直是正常的，最近换了新的证书，有部分电脑报证书问题

第一步

#### 排查1

通过看看客户那边的日志，看到日志发现报了如下错误

 ```
 x509: certificate signed by unknown authority
 ```

IE 访问项目后台地址，报错误，提示证书不安全。

确定，是终端没有证书，所以把证书在终端安装，包括根证书和中级颁发机构证书。

不分电脑，安装了以后是正常了，可能还有部分电脑依然有问题。

####  排查2

电脑依然用不了的，继续看了下终端打印的错误日志

```
http : TLS handshake timeout
```

一度以为是电脑缺啥东西了，系统本身问题，最后发现其实也就是系统缺少东西。然后就让门店重做系统，换win10的系统。

由于安装的证书是DigiCert Global Root G2 所以，搜索了下这个，有发现如下结果

![image-20231115140241782](C:\Users\a\AppData\Roaming\Typora\typora-user-images\image-20231115140241782.png)

进入第二个链接，里面说的很详细，为什么会出问题，怎么解决，最后找到这个系统更新

最后的结果是，需要安装一个系统更新包，系统更新包可以在微软官网下载，搜索这个

```
Windows Root Certificate Program
```

在这里下载

```
https://support.microsoft.com/en-us/topic/support-for-urgent-trusted-root-updates-for-windows-root-certificate-program-in-windows-a4ac4d6c-7c62-3b6e-dfd2-377982bf3ea5
```

![image-20231115140820010](C:\Users\a\AppData\Roaming\Typora\typora-user-images\image-20231115140820010.png)

根据自己的系统版本下载