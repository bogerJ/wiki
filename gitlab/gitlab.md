### 镜像下载

gitlab-ce 镜像下载地址

 https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7/

### 2.3 gitlab的配置

配置文件位置 /etc/gitlab/gitlab.rb

```
[root@centos7 test]# vim /etc/gitlab/gitlab.rb

[root@centos7 test]# grep "^[a-Z]" /etc/gitlab/gitlab.rb

external_url 'http://10.0.0.51'  # 这里一定要加上http://

# 配置邮件服务
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "smtp.qq.com"
gitlab_rails['smtp_port'] = 25
gitlab_rails['smtp_user_name'] = "hgzerowzh@qq.com"  # 自己的qq邮箱账号
gitlab_rails['smtp_password'] = "xxx"  # 开通smtp时返回的授权码
gitlab_rails['smtp_domain'] = "qq.com"
gitlab_rails['smtp_authentication'] = "login"   
gitlab_rails['smtp_enable_starttls_auto'] = true
gitlab_rails['smtp_tls'] = false
gitlab_rails['gitlab_email_from'] = "hgzerowzh@qq.com"  # 指定发送邮件的邮箱地址
user["git_user_email"] = "shit@qq.com"   # 指定接收邮件的邮箱地址
```

**修改好配置文件后，要使用 gitlab-ctl reconfigure 命令重载一下配置文件，否则不生效。**

```
gitlab-ctl reconfigure # 重载配置文件
```

#### 修改端口







### 2.4 Gitlab常用命令

```
gitlab-ctl start         # 启动所有 gitlab 组件
gitlab-ctl stop          # 停止所有 gitlab 组件
gitlab-ctl restart       # 重启所有 gitlab 组件
gitlab-ctl status        # 查看服务状态

gitlab-ctl reconfigure   # 启动服务
gitlab-ctl show-config   # 验证配置文件

gitlab-ctl tail          # 查看日志

gitlab-rake gitlab:check SANITIZE=true --trace    # 检查gitlab

 vim /etc/gitlab/gitlab.rb # 修改默认的配置文件
```