## linux + nginx 高并发配置

最近在做的服务，后端服务通过linux 进行负，在做压力测试的时候并发达到 1000 的时候nginx 返回500，查看nginx 错误日志发现报错

> socket() failed (24: Too many open files) while connecting to upstream

#### linux 配置

打开太多文件了，首先想到的是linux服务器打开文件数的限制，先查下

~~~shell
[root@instance-74y9u56h-2 ~]# ulimit -a
core file size          (blocks, -c) 0
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 31846
max locked memory       (kbytes, -l) 64
max memory size         (kbytes, -m) unlimited
open files                      (-n) 65535
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 31846
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited
~~~

open files  打开文件输的限制，已经改过的 65535 没问题，继续测试，查看服务器在峰值的时候打开的文件数

~~~
lsof |wc -l
~~~

发现打开的文件数并没有达到限制的数量。

#### nginx 配置

1. work_rlimit_nofile 

   这个配置是 nginx 打开文件数的限制，默认不知道多少

~~~
work_rlimit_nofile 65535;
~~~

修改nginx 的完成配置如下

~~~nginx
#user  nobody;
worker_processes 2 ; # 
worker_rlimit_nofile 65536 ; nginx 打开的文件数量限制

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;

events {
    worker_connections  32768; #
    # multi_accept on;
}
### 其他省略
~~~

修改后，重启nginx 问题解决，在继续加负载测试