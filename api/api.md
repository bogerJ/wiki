### 棉包检验数据接口

##### 版本 V1.0

* 接口说明

  接口请求，传参为json 格式

* 服务器地址

  ```
  ......
  ```

* 接口地址

  ```
  admin/cottonCheckInfo/getList
  ```

* 请求头

  | 参数         | 值               | 说明 |
  | ------------ | ---------------- | ---- |
  | Content-Type | application/json | 固定 |

* 入参

  | 参数             | 类型   | 示例        | 备注       |
  | ---------------- | ------ | ----------- | ---------- |
  | processBatchCode | string | 65287201049 | 批号，必填 |
  | page             | int    | 1           | 固定，必填 |
  | pageSize         | int    | 10          | 固定，必填 |

* 返回示例

  ```
  {
      "code": 200,
      "data": {
      	"list":[bean...]
      },
      "msg": "操作成功"
  }
  ```

* bean

  ```go
  type CottonCheckInfo struct {
  	Model
  	AvgBreakRate         float64 `json:"avgBreakRate"`         //平均断裂比强度
  	AvgLength            float64 `json:"avgLength"`            //平均长度
  	AvgLengthUnifo       float64 `json:"avgLengthUnifo"`       //平均整齐度
  	AvgMkl               float64 `json:"avgMkl"`               //平均马值
  	AvgPlusb             float64 `json:"avgPlusb"`             //平均 B+
  	AvgRd                float64 `json:"avgRd"`                //平均Rd
  	BreakrateS1          float64 `json:"breakrateS1"`          //S1占比
  	BreakrateS2          float64 `json:"breakrateS2"`          //
  	BreakrateS3          float64 `json:"breakrateS3"`          //
  	BreakrateS4          float64 `json:"breakrateS4"`          //
  	BreakrateS5          float64 `json:"breakrateS5"`          //
  	ColorGrade           string  `json:"colorGrade"`           //颜色级
  	EnterPriseCode       string  `json:"enterPriseCode"`       //加工厂编号
  	EnterpriseType       string  `json:"enterpriseType"`       //
  	ForeFiber            float64 `json:"foreFiber"`            //
  	GinningqualityrateP1 float64 `json:"ginningqualityrateP1"` //轧工质量 p1
  	GinningqualityrateP2 float64 `json:"ginningqualityrateP2"` // p2
  	GinningqualityrateP3 float64 `json:"ginningqualityrateP3"` // p3
  	GrossWeight          float64 `json:"grossWeight"`          //合计毛重
  	ImpurityRate         float64 `json:"impurityRate"`         //含杂率
  	InspectDate          string  `json:"inspectDate"`          //检验日期
  	InspectStatus        string  `json:"inspectStatus"`        //检验状态
  	LengthRate25         float64 `json:"lengthRate25"`         //25 长度占比
  	LengthRate26         float64 `json:"lengthRate26"`         //26
  	LengthRate27         float64 `json:"lengthRate27"`         //27
  	LengthRate28         float64 `json:"lengthRate28"`         //28
  	LengthRate29         float64 `json:"lengthRate29"`         //29
  	LengthRate30         float64 `json:"lengthRate30"`         //30
  	LengthRate31         float64 `json:"lengthRate31"`         //31
  	LengthRate32         float64 `json:"lengthRate32"`         //32
  	LenuniformityRateU1  float64 `json:"lenuniformityRateU1"`  //
  	LenuniformityRateU2  float64 `json:"lenuniformityRateU2"`  //
  	LenuniformityRateU3  float64 `json:"lenuniformityRateU3"`  //
  	LenuniformityRateU4  float64 `json:"lenuniformityRateU4"`  //
  	LenuniformityRateU5  float64 `json:"lenuniformityRateU5"`  //
  	Level1Rate           float64 `json:"level1Rate"`           // 1级 占比
  	Level2Rate           float64 `json:"level2Rate"`           //
  	Level3Rate           float64 `json:"level3Rate"`           //
  	Level4Rate           float64 `json:"level4Rate"`           //
  	Level5Rate           float64 `json:"level5Rate"`           //
  	Level6Rate           float64 `json:"level6Rate"`           //
  	Level7Rate           float64 `json:"level7Rate"`           //
  	LiterDiscount        float64 `json:"literDiscount"`        //
  	MainColorGrade       string  `json:"mainColorGrade"`       // 主颜色级
  	MainLength           string  `json:"mainLength"`           // 主长度
  	MainMkl              string  `json:"mainMkl"`              // 主 马值
  	MaxBreakRate         float64 `json:"maxBreakRate"`         // 最大强度
  	MaxLengthUnifo       float64 `json:"maxLengthUnifo"`       // 最大长度整齐度
  	MinBreakRate         float64 `json:"minBreakRate"`         // 最小强度
  	MinLengthUnifo       float64 `json:"minLengthUnifo"`       // 最小长度整齐度
  	MklA                 float64 `json:"mklA"`                 //
  	MklB1                float64 `json:"mklB1"`                //
  	MklB2                float64 `json:"mklB2"`                //
  	MklC1                float64 `json:"mklC1"`                //
  	MklC2                float64 `json:"mklC2"`                //
  	MoistureRate         float64 `json:"moistureRate"`         //含水率  湿度
  	NetWeight            float64 `json:"netWeight"`            //净重
  	PacketNum            float64 `json:"packetNum"`            //包数
  	ProcessBatchCode     string  `json:"processBatchCode"`     //批号
  	ProcessEnterprice    string  `json:"processEnterprice"`    //
  	ProcessType          string  `json:"processType"`          //
  	ProductName          string  `json:"productName"`          //棉花类型
  	Productarea          string  `json:"productarea"`          //
  	PubWeight            float64 `json:"pubWeight"`            // 公重
  	PublishDate          string  `json:"publishDate"`          //
  	Repository           string  `json:"repository"`           // 仓库
  	SalesPrice           float64 `json:"salesPrice"`           // 售价
  	SpotCottonL1         float64 `json:"spotCottonL1"`         // 淡点污棉1级 12
  	SpotCottonL2         float64 `json:"spotCottonL2"`         //  22
  	SpotCottonL3         float64 `json:"spotCottonL3"`         //  32
  	Status               string  `json:"status"`               //
  	SubjectLevel         string  `json:"subjectLevel"`         //
  	TareWeight           float64 `json:"tareWeight"`           //皮重
  	WeighStatus          string  `json:"weighStatus"`          //
  	WhiteCottonL1        float64 `json:"whiteCottonL1"`        //白棉 11
  	WhiteCottonL2        float64 `json:"whiteCottonL2"`        // 21
  	WhiteCottonL3        float64 `json:"whiteCottonL3"`        // 31
  	WhiteCottonL4        float64 `json:"whiteCottonL4"`        // 41
  	WhiteCottonL5        float64 `json:"whiteCottonL5"`        // 51
  	WorkYear             string  `json:"workYear"`             //年度
  	YellowCottonL1       float64 `json:"yellowCottonL1"`       //黄棉 14
  	YellowCottonL2       float64 `json:"yellowCottonL2"`       // 24
  	YellowishCottonL1    float64 `json:"yellowishCottonL1"`    //淡黄棉 13
  	YellowishCottonL2    float64 `json:"yellowishCottonL2"`    // 23
  	YellowishCottonL3    float64 `json:"yellowishCottonL3"`    // 33
  	ZmxDiscount          float64 `json:"zmxDiscount"`          //棉协升贴水
  	ZssDiscount          float64 `json:"zssDiscount"`          //郑棉升贴水
  	CType int         `json:"cType"` // 类型  1 新疆棉 2 内地 3 进口棉 4 储拍
  	BasicDiffPrice       float64 `json:"basicDiffPrice" gorm:"-"`
  	ProdStatus           int     `json:"prodStatus" gorm:"-"`
  	SaleTimes            int     `json:"saleTimes" gorm:"-"`
  	ProdOwner            string  `json:"prodOwner" gorm:"-"`
  	OwnerTel             string  `json:"ownerTel" gorm:"-"`
  	PriceMode int `json:"priceMode" gorm:"-"`
  }
  ```

  