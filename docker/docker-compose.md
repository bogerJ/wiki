1 安装

```shell
curl -L https://get.daocloud.io/docker/compose/releases/download/1.25.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
```

2 语法	

docker-compose up -d

```
docker-compose up
用途： 启动并运行 docker-compose.yml 文件中定义的所有服务。

docker-compose up

常用选项：

 -d: 后台运行容器。
 --build: 强制重新构建服务的镜像。
 <service_name>: 启动特定服务，而不是所有服务。
```

3 例子

```yml
version: "3.5"
services:
  etcd:
    hostname: etcd
    image: bitnami/etcd:3
    deploy:
      replicas: 1
      restart_policy:
        condition: on-failure
    # ports:
    #   - "2379:2379"
    #   - "2380:2380"
    #   - "4001:4001"
    #   - "7001:7001"
    user: root
    volumes:
      - "/var/docker/etcd/data:/opt/bitnami/etcd/data"
    environment:
      - "ETCD_ADVERTISE_CLIENT_URLS=http://etcd:2379"
      - "ETCD_LISTEN_CLIENT_URLS=http://0.0.0.0:2379"
      - "ETCD_LISTEN_PEER_URLS=http://0.0.0.0:2380"
      - "ETCD_INITIAL_ADVERTISE_PEER_URLS=http://0.0.0.0:2380"
      - "ALLOW_NONE_AUTHENTICATION=yes"
      - "ETCD_INITIAL_CLUSTER=node1=http://0.0.0.0:2380"
      - "ETCD_NAME=node1"
      - "ETCD_DATA_DIR=/opt/bitnami/etcd/data"
  # 方式1 通过环境变量
  micro:
    hostname: micro1
    image: micro:latest 
    ports: 
      - "8080:8080"
      - "8082:8082"
    environment:
      - "MICRO_REGISTRY=etcd"
      - "MICRO_REGISTRY_ADDRESS=etcd:6379"
    links:
      - etcd
  
  # 方式二 通过命令行传参
  micro2:
    command: --registry_address=etcd:2379 --resistry=etcd api
    hostname: micro2
    image: micro:latest 
    ports: 
      - "18080:8080"
      - "18082:8082"
    links:
      - etcd

```

