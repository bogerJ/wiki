

1、安装依赖

docker依赖于系统的一些必要的工具，可以提前安装。

```
yum install -y yum-utils device-mapper-persistent-data lvm2
```

2、添加软件源

```
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

3、安装docker-ce

```
yum clean all && yum makecache fast && yum -y install docker-ce
```

