### 阿里云镜像加速器

登录阿里云平台，访问容器镜像服务，找到镜像加速器



```{
#/etc/docker/daemon.json
{
 "registry-mirrors": ["https://iztwcvx3.mirror.aliyuncs.com"]
}
```



开启管理端口

查看docker 状态，可以在 查出来的信息中找到docker 服务文件的地址

```shell
[root@vm12345-0417747 ~]# systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-06-11 11:04:48 CST; 1min 9s ago
     Docs: https://docs.docker.com
 Main PID: 15150 (dockerd)
    Tasks: 10
   Memory: 39.8M
   CGroup: /system.slice/docker.service
           └─15150 /usr/bin/dockerd -H tcp://0.0.0.0:12375 -H unix:///var/run/docker.sock

```

docker服务文件： /usr/lib/systemd/system/docker.service

修改docker服务文件,修改 ExecStart 的内容为	

```shell
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
```

systemctl daemon-reload 重新加载配置

systemctl restart docker 	重启服务







