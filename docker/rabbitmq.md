rabbitmq docker 安装

```shell
docker run -d it --name Myrabbitmq -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```

> 下面安装的是 rabbitmq:3-management具体的版本可以到docker hub 上找找
> RABBITMQ_DEFAULT_USER 默认的 用户名
> RABBITMQ_DEFAULT_PASS 默认密码
> 15672:15672 管理端口映射
> 5672:5672 消息端口映射

[http://127.0.0.1:15672](http://127.0.0.1:15672/)



elk

```shell
 docker run -d -p 5601:5601 -p 9200:9200 -p 5044:5044 -e ES_MIN_MEM=128m -e ES_MAX_MEM=1024m --name elk sebp/elk
```



